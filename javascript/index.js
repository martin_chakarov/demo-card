$(function () {

    $('.dosageDropdown').select2({
        placeholder: 'Please Select',
        minimumResultsForSearch: -1
    })

    let $amountIcons = $('.icon');

    $amountIcons.click(amountIconsHandler);

    let $amountInput = $('.amount');

    $amountInput.on('input', validateInput);
    $amountInput.on('input', updatePrice);

    let $optionButtons = $('.optionsBtn');

    $optionButtons.click(changeViewHandler);


    function amountIconsHandler(e) {

        let eventTarget = e.target;

        let amountNumber = parseInt($amountInput.val());

        if ($(eventTarget).hasClass('plus') || $(eventTarget).hasClass('fa-plus')) {

            if (amountNumber + 1 == 1000) {
                return;
            }
            else {
                $amountInput.val(amountNumber + 1);
                updatePrice();
            }
        }
        else {

            if (amountNumber - 1 == -1) {
                return;
            }
            else {
                $amountInput.val(amountNumber - 1);
                updatePrice();
            }
        }
    }

    function updatePrice() {

        let $priceAmount = $('.price');

        let singleProductPrice = 1000;
        let qty = $('.amount').val();

        let updatedPrice = singleProductPrice * qty;

        $priceAmount.text(updatedPrice.toFixed(2).replace('.', ','))
    }

    function validateInput(e) {

        let value = Number(e.target.value);

        if (!Number.isInteger(value)){
            $amountInput.val(Math.floor(value));
        }

        if (value > 999) {
            $amountInput.val(999);
            updatePrice();
        }
        if (value < 0) {
            $amountInput.val(0);
            updatePrice();
        }
    }

    function changeViewHandler(e) {
        toggleClickedButton(e);
        let view = e.target.textContent.split(' ')[0].toLocaleLowerCase();
        let $shoppingCartIconContainer = $('.shoppingCartIcon');
        let $drugCard = $('.drugCard');

        $shoppingCartIconContainer.find('i').removeClass('fa-exclamation').addClass('fa-cart-arrow-down');

        if (view === 'normal'){
            $drugCard[0].classList = 'drugCard';
        }
        if (view === 'sale' && !$drugCard.hasClass('sale')) {
            $drugCard[0].classList = 'drugCard';
            $drugCard.toggleClass('sale');
        }
        if (view === 'out' && !$drugCard.hasClass('out-of-stock')) {
            $drugCard[0].classList = 'drugCard';
            $drugCard.toggleClass('out-of-stock');
            $shoppingCartIconContainer.find('i').removeClass('fa-cart-arrow-down').addClass('fa-exclamation');
        }
    }


    function toggleClickedButton(e){

       let $optionsBtns = $('.optionsBtn');

        if ($optionsBtns.hasClass('toggled')){
            $optionButtons.removeClass('toggled');
        }

        $(e.target).addClass('toggled');
    }
});